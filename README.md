This project gather all configuration files and programs deployable on a Raspberry Pi to monitor and analyse network traffics with a tap.

Many information are available on the wiki (en and fr) : https://framagit.org/network-probe/raspberry-pi/wikis/home

This project relies on https://framagit.org/network-probe/tap.